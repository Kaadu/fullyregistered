from urllib.request import urlopen
import json

# Get the anonymous submissions
data = urlopen("http://www.rcfv.com.au/api/Submission/List.ashx?ano=1").read()

data = json.loads(data.decode())

for rec in data['data']:
    try:
        print("http://www.rcfv.com.au/" + rec['submissionfilepath'])
        file = urlopen("http://www.rcfv.com.au/" + rec['submissionfilepath'])
        f = open("./out/" + rec['submissionfilepath'].split('/')[-1], "+wb")
        f.write(file.read())
        f.close()
    except:
        pass

# Get the non anonymous submission
data = urlopen("http://www.rcfv.com.au/api/Submission/List.ashx").read()

data = json.loads(data.decode())

for rec in data['data']:
    try:
        print("http://www.rcfv.com.au/" + rec['submissionfilepath'])
        file = urlopen("http://www.rcfv.com.au/" + rec['submissionfilepath'])
        f = open("./out/" + rec['submissionfilepath'].split('/')[-1], "+wb")
        f.write(file.read())
        f.close()
    except:
        pass