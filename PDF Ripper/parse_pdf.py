import json
from os import listdir
from os.path import isfile, join

import slate

files = [join("./out/", f) for f in listdir("./out/") if isfile(join("./out/", f))]

data = {}
for f in files:
    print(f)
    try:
        o = open(f)
        data[f] = slate.PDF(o)
        o.close()
    except:
        pass

    out_data = open("text.json", "w")
    json.dump(data, out_data)
    out_data.close()


