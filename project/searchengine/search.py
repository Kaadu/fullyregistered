from fullyregistered.models import OriginalSubmission


def search(term):
    metadata = OriginalSubmission.objects.all()
    for data in metadata:
        tokens = data.submission_name.split(' ')
        if term in tokens:
            return data.originalsubmissionpage_set.all()