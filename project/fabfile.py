__author__ = 'Lewis'

# Please not that you need to have the cloud.ppk file loaded in putty pageant in order to deploy to the server...
# Please email ilewis_isradd@hotmail.com for a copy of the key.

# You will also need 7zip installed locally.

# Import the Fabric api
from fabric.api import *
import platform

# Specify our host
env.user = 'root'
env.hosts = ['root@retsim.com']

staging_directory = "c:\\Temp\\"
staging_file_name = "Staging.zip"

remote_temp_path = "/tmp/"
remote_running_dir = "/var/www/fullyregistered/"

del_cmd = "del"

print platform.system()

if platform.system() == "Linux":
    staging_directory = "/tmp/"
    del_cmd = "rm"

# Where your project code lives on the server
env.project_root = remote_running_dir

def deploy():
    # del might fail if the staging file does not exist...
    with settings(warn_only=True):
        local(del_cmd + " " + staging_directory + staging_file_name)

    local("7z a " + staging_directory + staging_file_name + " -xr!.hg -xr!.idea -xr!*.pyc -r")
    put(staging_directory + staging_file_name, remote_temp_path)

    with settings(warn_only=True):
        run("rm -f -R " + remote_running_dir + "*.py")
        run("rm -f -R " + remote_running_dir + "*.pyc")

    run("unzip -q -o " + remote_temp_path + staging_file_name + " -d " + remote_running_dir)

    with cd(env.project_root):
            run('python manage.py collectstatic -v0 --noinput')
            run('python manage.py migrate')

    run("chown -R apache:apache " + remote_running_dir)
    run("service apache2 restart")