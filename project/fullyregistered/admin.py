from django.contrib import admin

from fullyregistered.models import OriginalSubmission, OriginalSubmissionPage, CrimeRecord, Address, PhoneNumber
from fullyregistered.models import WordModelPerson, WordModelDate
from fullyregistered.models import HealthService, HealthServiceArea

class OriginalSubmissionAdmin(admin.ModelAdmin):
    list_display = ('submission_name', 'original_pdf_url', 'preview', 'getPeople')
    search_fields = ['name']
admin.site.register(OriginalSubmission,OriginalSubmissionAdmin)

class WordModelPersonInline(admin.TabularInline):
    model = WordModelPerson
    extra = 1
class WordModelDateInline(admin.TabularInline):
    model = WordModelDate
    extra = 1
class OriginalSubmissionPageAdmin(admin.ModelAdmin):
    inlines = (WordModelPersonInline,WordModelDateInline)
admin.site.register(OriginalSubmissionPage,OriginalSubmissionPageAdmin)

class CrimeRecordAdmin(admin.ModelAdmin):
    list_display = ('region', 'area', 'stats_2015','position')
    list_filter = ['region']
    search_fields = ['area']
    list_display_links = ['area']
admin.site.register(CrimeRecord,CrimeRecordAdmin)
admin.site.register(Address)
admin.site.register(PhoneNumber)

class WordModelPersonAdmin(admin.ModelAdmin):
    list_display = ('person', 'submission')
    search_fields = ['area','parent']
admin.site.register(WordModelPerson,WordModelPersonAdmin)
class WordModelDateAdmin(admin.ModelAdmin):
    list_display = ('date', 'submission', 'is_day_known', 'is_year_known')
    search_fields = ['area','parent']
admin.site.register(WordModelDate,WordModelDateAdmin)

class HealthServiceAreaInline(admin.TabularInline):
    model = HealthServiceArea
    extra = 1

class HealthServiceAdmin(admin.ModelAdmin):
    inlines = [HealthServiceAreaInline]
    list_display = ('region', '__str__', 'is_aboriginal')
    list_filter = ['region', 'is_aboriginal']
    search_fields = ['__str__']
admin.site.register(HealthService, HealthServiceAdmin)
class HealthServiceAreaAdmin(admin.ModelAdmin):
    list_display = ('area', 'health_service','position')
    list_filter = ['health_service']
    search_fields = ['area']
admin.site.register(HealthServiceArea,HealthServiceAreaAdmin)