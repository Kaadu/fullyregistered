from django.forms import FileField, Form, CharField


class SubmissionUploadForm(Form):
    file = FileField()


class SearchForm(Form):
    search_terms = CharField()
