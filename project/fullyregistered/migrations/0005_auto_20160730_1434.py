# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-30 04:34
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fullyregistered', '0004_crimerecord'),
    ]

    operations = [
        migrations.CreateModel(
            name='WordModelDate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('submissionPage', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fullyregistered.OriginalSubmissionPage')),
            ],
        ),
        migrations.CreateModel(
            name='WordModelIncident',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('incident', models.CharField(max_length=200)),
                ('submissionPage', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fullyregistered.OriginalSubmissionPage')),
            ],
        ),
        migrations.CreateModel(
            name='WordModelOrganisation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('organisation', models.CharField(max_length=200)),
                ('submissionPage', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fullyregistered.OriginalSubmissionPage')),
            ],
        ),
        migrations.CreateModel(
            name='WordModelPerson',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('person', models.CharField(max_length=200)),
                ('submissionPage', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fullyregistered.OriginalSubmissionPage')),
            ],
        ),
        migrations.AddField(
            model_name='originalsubmission',
            name='submitter',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
