# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django.db import migrations, models


def do_migration(apps, schema_editor):
    from urllib.request import urlopen

    OriginalSubmission = apps.get_model("fullyregistered", "OriginalSubmission")

    # Get the anonymous submissions
    data = urlopen("http://www.rcfv.com.au/api/Submission/List.ashx?ano=1").read()

    data = json.loads(data.decode())

    for rec in data['data']:
        for s in OriginalSubmission.objects.filter(submission_name=rec['submissionfilepath'].split('/')[-1]):
            s.original_pdf_url = "http://www.rcfv.com.au" + rec['submissionfilepath']
            s.save()
            print(s.original_pdf_url)

    # Get the non anonymous submission
    data = urlopen("http://www.rcfv.com.au/api/Submission/List.ashx").read()

    data = json.loads(data.decode())

    for rec in data['data']:
        for s in OriginalSubmission.objects.filter(submission_name=rec['submissionfilepath'].split('/')[-1]):
            s.original_pdf_url = "http://www.rcfv.com.au" + rec['submissionfilepath']
            s.save()
            print(s.original_pdf_url)


class Migration(migrations.Migration):

    dependencies = [
        ('fullyregistered', '0011_originalsubmission_original_pdf_url'),
    ]

    operations = [
        migrations.RunPython(do_migration),
    ]