# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-29 22:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fullyregistered', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OriginalSubmissionPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('page_content', models.TextField()),
            ],
        ),
        migrations.RemoveField(
            model_name='originalsubmission',
            name='submission_content',
        ),
        migrations.AddField(
            model_name='originalsubmissionpage',
            name='submission',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fullyregistered.OriginalSubmission'),
        ),
    ]
