import json
import csv
import datetime
import re

from django.shortcuts import render_to_response, render, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse


from fullyregistered.forms import SubmissionUploadForm, SearchForm
from fullyregistered.models import OriginalSubmission, OriginalSubmissionPage, CrimeRecord, WordModelPerson, PhoneNumber, \
    Address
from fullyregistered.models import OriginalSubmission, OriginalSubmissionPage, CrimeRecord, WordModelPerson, WordModelDate
from fullyregistered.models import HealthService, HealthServiceArea


from fullyregistered import settings

from fullyregistered import WordModel


def home(request):
    return render_to_response('index.html', {}, content_type=RequestContext(request))


def upload_submissions(request):
    title = 'Submissions'

    if request.method == 'POST':
        form = SubmissionUploadForm(request.POST, request.FILES)
        if form.is_valid():
            data = json.loads(request.FILES['file'].read().decode())
            OriginalSubmission.objects.all().delete()
            for k in data:
                submission = OriginalSubmission()
                submission.submission_name = k.split('/')[-1]
                submission.save()
                idx = 0
                for p in data[k]:
                    page = OriginalSubmissionPage()
                    page.submission = submission
                    page.page_content = p
                    page.index = idx
                    idx += 1
                    page.save()
    else:
        form = SubmissionUploadForm()

    return render_to_response('upload_submissions.html', {'form': form, 'title': title}, context_instance=RequestContext(request))


def map_api(request):
    phone_numbers = PhoneNumber.objects.all()
    addresses = Address.objects.all()
    crimes = CrimeRecord.objects.all()
    health_areas = HealthServiceArea.objects.all()

    if 'term' in request.GET:
        submissions = {}

        for p in search_terms(request.GET['term'].lower().split(' ')):
            submissions[p['title']] = p['submission']

        phone_numbers = PhoneNumber.objects.none()
        addresses = Address.objects.none()

        for k in submissions.keys():
            phone_numbers |= PhoneNumber.objects.filter(submission=submissions[k])
            addresses |= Address.objects.filter(submission=submissions[k])

    lat_lons = {}
    crime_lats = {}
    for number in phone_numbers:
        combo = str(number.lat) + "," + str(number.lon)
        if combo in lat_lons:
            lat_lons[combo]['number'] += 1
        else:
            lat_lons[combo] = {'number': 1, 'lat': number.lat, 'lon': number.lon, 'submission': number.submission.original_pdf_url}

    for address in addresses:
        combo = str(address.lat) + "," + str(address.lon)
        if combo in lat_lons:
            lat_lons[combo]['number'] += 1
        else:
            lat_lons[combo] = {'number': 1, 'lat': address.lat, 'lon': address.lon, 'submission': address.submission.original_pdf_url}

    for crime in crimes:
        combo = str(crime.lat) + "," + str(crime.lon)
        crime_lats[combo] = {'number': crime.incidents_2015, 'lat': crime.lat, 'lon': crime.lon}

    mini = 999999
    maxi = -999999
    for c in crime_lats.keys():
        if crime_lats[c]['number'] > maxi:
            maxi = crime_lats[c]['number']
        if crime_lats[c]['number'] < mini:
            mini = crime_lats[c]['number']

        crime_lats[c]['original_number'] = crime_lats[c]['number']

    for c in crime_lats.keys():
        crime_lats[c]['number'] = (crime_lats[c]['number'] - mini) * (100.0/(maxi-mini)) + 10.0

    mini = 999999
    maxi = -999999
    for c in lat_lons.keys():
        if lat_lons[c]['number'] > maxi:
            maxi = lat_lons[c]['number']
        if lat_lons[c]['number'] < mini:
            mini = lat_lons[c]['number']

        lat_lons[c]['original_number'] = lat_lons[c]['number']

    for c in lat_lons.keys():
        lat_lons[c]['number'] = (lat_lons[c]['number'] - mini) * (100.0/(maxi-mini)) + 10.0

    health_lats = {}
    for health_area in health_areas:
        combo = str(health_area.lat) + "," + str(health_area.lon)
        if combo in health_lats:
            health_lats[combo]['number'] += 1
        else:
            health_lats[combo] = {'number': 1, 'lat': health_area.lat, 'lon': health_area.lon}

    for c in health_lats.keys():
        if health_lats[c]['number'] > maxi:
            maxi = health_lats[c]['number']
        if health_lats[c]['number'] < mini:
            mini = health_lats[c]['number']

        health_lats[c]['original_number'] = health_lats[c]['number']

    for c in health_lats.keys():
        health_lats[c]['number'] = (health_lats[c]['number'] - mini) * (100 / (maxi - mini)) + 10

    return render_to_response('map.html', {'api_key': settings.MAPS_API_KEY, 'locations': lat_lons, 'crime_locations': crime_lats, 'health_locations' : health_lats}, context_instance=RequestContext(request))


def import_rates_of_violence(request):
    title = 'Rates of Violence CSV file'

    if request.method == 'POST':
        form = SubmissionUploadForm(request.POST, request.FILES)
        if form.is_valid():
            csvreader = csv.reader(str(request.FILES['file'].read().decode()).splitlines())

            CrimeRecord.objects.all().delete()

            first = True
            for row in csvreader:
                # Skip the header
                if first:
                    first = False
                    continue

                rec = CrimeRecord()
                rec.region = row[0]
                rec.area = row[1]
                rec.incidents_2011 = int(row[2])
                rec.incidents_2012 = int(row[3])
                rec.incidents_2013 = int(row[4])
                rec.incidents_2014 = int(row[5])
                rec.incidents_2015 = int(row[6])
                rec.rate_2011 = float(row[7])
                rec.rate_2012 = float(row[8])
                rec.rate_2013 = float(row[9])
                rec.rate_2014 = float(row[10])
                rec.rate_2015 = float(row[11])
                rec.save()
    else:
        form = SubmissionUploadForm()

    return render_to_response('upload_submissions.html', {'form': form, 'title': title},
                              context_instance=RequestContext(request))

def import_health_services(request):
    title = 'Victorian Health Services csv'

    if request.method == 'POST':
        form = SubmissionUploadForm(request.POST, request.FILES)
        if form.is_valid():
            #does not like funny ' and funny -
            csvreader = csv.reader(str(request.FILES['file'].read().decode()).splitlines())

            HealthService.objects.all().delete()

            first = True
            for row in csvreader:
                # Skip the header
                if first:
                    first = False
                    continue

                if row[2]:
                    rec = HealthService()
                    rec.region = row[0]
                    rec.name = row[1]
                    rec.save()
                    for i in row[2].split('|'):
                        recChild =  HealthServiceArea()
                        recChild.health_service = rec
                        recChild.area = i
                        recChild.save()

                    if row[3]:
                        rec.is_aboriginal = True
                    else:
                        rec.is_aboriginal = False

                    rec.save()
    else:
        form = SubmissionUploadForm()

    return render_to_response('upload_submissions.html', {'form': form, 'title': title},
                              context_instance=RequestContext(request))


def display_data(request):

    return render_to_response('stats.html', {}, context_instance=RequestContext(request))


# extracts summary data from original submissions
def process_person_name(request):
    WordModelPerson.objects.all().delete()
    submission = OriginalSubmission.objects.all()  # list of original submission objects

    for item in submission:
        WordModel.extract_person_name(item)
    return HttpResponse('Complete! at ' + datetime.datetime.now().strftime('%X'))


def process_dates(request):
    WordModelDate.objects.all().delete()
    submission = OriginalSubmission.objects.all()
    for item in submission:
        WordModel.find_date(item)

    return HttpResponse('Complete! at ' + datetime.datetime.now().strftime('%X'))


def process_identifiers(request):
    submission = OriginalSubmission.objects.all()
    for item in submission:
        WordModel.process_identifiers(item)

    return HttpResponse('Complete! at ' + datetime.datetime.now().strftime('%X'))


def test(request):
    WordModelPerson.objects.all().delete()

    submission = OriginalSubmission.objects.get(submission_name='Anonymous-195')
    #WordModel.extract_entity(submission)

    WordModel.extract_incident(submission)
    #
    # pages, words = summary.count_words()
    # print(pages, words)
    #
    # total_page = 0
    # total_words = 0
    #
    # for key, sub in data.items():
    #     summary = WordModel(sub)
    #     summary.extract_entity()
    #
    #     pages, words = summary.count_words()
    #
    #     # word count
    #     total_page += pages
    #     total_words += words
    #     print("Pages: " + str(total_page) + ", Words: \n")
    return HttpResponse('Complete!')


def search_terms(terms):
    pages = OriginalSubmissionPage.objects.all()
    pages = [{
                 'id': p.id,
                 'url': p.submission.original_pdf_url,
                 'sub_id': p.submission_id,
                 'title': p.submission.submission_name,
                 'identifer': p.submission.submission_identifier,
                 'people': p.submission.getPeople,
                 'dates': p.submission.find_date_range,
                 'text': p.page_content.lower(),
                 'submission': p.submission
             } for p in pages]

    for term in terms:
        pages = [{
                     'id': p['id'],
                     'url': p['url'],
                     'sub_id': p['sub_id'],
                     'title': p['title'],
                     'identifer': p['identifer'],
                     'people': p['people'],
                     'dates': p['dates'],
                     'text': re.sub(r'_', ' ', re.sub(r'!(\x32-\x7F)|subm\.\d{4}\.\d{3}\.\d{4}|\|', '', p['text'])),
                     'submission': p['submission']

                 } for p in pages if term in p['text'] or term in p['title']]

    return pages


def search(request):
    search_form = SearchForm()

    if request.method == 'POST':
        submissions = {}
        terms = request.POST['search_terms'].lower()
        for p in search_terms(request.POST['search_terms'].lower().split(' ')):
            submissions[p['title']] = p
    else:
        submissions = None
        terms = None

    return render_to_response('search.html', {'search_form': search_form, 'pages': submissions, 'terms': terms}, context_instance=RequestContext(request))


def submission_view(request, id):
    submission = get_object_or_404(OriginalSubmission, id=id)
    return render_to_response('submission_view.html', {'submission': submission}, context_instance=RequestContext(request))


def test_dates(request):
    WordModelDate.objects.all().delete()

    submission = OriginalSubmission.objects.get(submission_name='Anonymous-182')
    WordModel.find_date(submission)
    return HttpResponse('Complete! at '+datetime.datetime.now().strftime('%X'))

def tetris(request):
    return render_to_response('tetris.html')