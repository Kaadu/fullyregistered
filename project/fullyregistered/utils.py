import re
from geopy import Nominatim
from pygeocoder import Geocoder
from django.http import HttpResponse

import pyap
from fullyregistered.models import OriginalSubmission, Address, PhoneNumber, CrimeRecord, HealthServiceArea

number_map = {
    '0332': 'Geelong',
    '0333': 'Ballarat',
    '0334': 'Bendigo',
    '0340': 'Mildura',
    '0341': 'Traralgon',
    '0342': 'Geelong',
    '0343': 'Ballarat',
    '0344': 'Bendigo',
    '0345': 'Warrnambool',
    '0347': 'Wangaratta',
    '0348': 'Deniliquin',
    '0349': 'Mornington',
    '0350': 'Mildura',
    '0351': 'Traralgon',
    '0352': 'Geelong',
    '0353': 'Ballarat',
    '0354': 'Bendigo',
    '0355': 'Warrnambool',
    '0356': 'Foster',
    '0357': 'Wangaratta',
    '0358': 'Deniliquin',
    '0359': 'Mornington',
    '0361': 'Hobart',
    '0362': 'Hobart',
    '0363': 'Launceston',
    '0364': 'Devonport',
    '0365': 'Devonport',
    '0367': 'Launceston',
    '037': 'Melbourne',
    '038': 'Melbourne',
    '039': 'Melbourne'
}

def process_locations(request):
    for s in OriginalSubmission.objects.all():
        m = pyap.parse(s.get_full_content(), country='AU')
        Address.objects.filter(submission=s).delete()
        for a in m:
            try:
                loc = Nominatim().geocode(a)
                if not loc:
                    continue

                if 'Australia' not in str(loc):
                    continue

                if Address.objects.filter(address=str(loc), submission=s).exists():
                    continue

                addr, _ = Address.objects.get_or_create(address=str(loc), submission=s, lat=loc.latitude, lon=loc.longitude)
                print("Found address for", s, "->", str(loc), addr.lat, addr.lon)
            except Exception as e:
                print(a, "was not a valid address.")

    return HttpResponse('ok')


def process_phone_numbers(request):
    for s in OriginalSubmission.objects.all():
        PhoneNumber.objects.filter(submission=s).delete()
        pn = re.findall(r'(?:\+?61|0)[2-478](?:[ -]?[0-9]){8}', s.get_full_content())
        for p in pn:
            try:
                p = p.replace(' ', '').replace('+61', '0')
                if p[:4] not in number_map and p[:3] not in number_map:
                    continue

                if p[:4] in number_map:
                    city = number_map[p[:4]]
                else:
                    city = number_map[p[:3]]

                if PhoneNumber.objects.filter(number=p, submission=s).exists():
                    continue

                loc = Nominatim().geocode(str(city))

                num, _ = PhoneNumber.objects.get_or_create(number=p, city=city, submission=s, lat=loc.latitude, lon=loc.longitude)
                print("Found phone number for", s, "->", p, city, num.lat, num.lon)
            except Exception as e:
                print(p, "was not a valid phone number.")

    return HttpResponse('ok')


def process_crime_areas(request):
    for s in CrimeRecord.objects.all():
        loc = Nominatim().geocode(s.area + " victoria australia")
        if not loc:
            continue

        s.lat = loc.latitude
        s.lon = loc.longitude
        s.save()

        print("Found address for", s.area, "->", str(loc), s.lat, s.lon)

    return HttpResponse('ok')

def process_health_areas(request):
    for s in HealthServiceArea.objects.all():
        loc = Nominatim().geocode(s.area + " victoria australia")
        if not loc:
            print(s.area + 'did not find any location')
            continue

        s.lat = loc.latitude
        s.lon = loc.longitude
        s.save()

        #print("Found address for", s.area, "->", str(loc), s.lat, s.lon)

    return HttpResponse('ok')