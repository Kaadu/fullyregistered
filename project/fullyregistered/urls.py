"""fullyregistered URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from fullyregistered.views import upload_submissions, map_api, upload_submissions, import_rates_of_violence, \
    display_data

from fullyregistered.utils import process_locations, process_phone_numbers, process_crime_areas, process_health_areas
from fullyregistered.views import upload_submissions
from fullyregistered.views import import_rates_of_violence
from fullyregistered.views import map_api, home
from fullyregistered.views import test, test_dates

from fullyregistered.views import *


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^adsfkjasdflkjoimewo!sdljif9mxokm3-werniniwo$', home, name='home'),

    # Submission Url
    url(r'^upload_submissions/$', upload_submissions, name='upload_submissions'),
    url(r'^map/$', map_api, name='map_api'),
    url(r'^stats/$', display_data, name='stats'),

    # Search Submissions
    url(r'^$', search, name='search'),
    url('^submission/(?P<id>[0-9]+)/$', submission_view, name='submission'),

    # csv uploader
    url(r'^csv_upload/$', import_rates_of_violence, name='csv_upload'),

    # csv import health service
    url(r'^import_health_services/$', import_health_services, name='health_service'),

    # process_locations
    url(r'^process_locations/$', process_locations, name='process_locations'),

    # process_phone_numbers
    url(r'^process_phone_numbers/$', process_phone_numbers, name='process_phone_numbers'),

    # process_phone_numbers
    url(r'^process_crime_areas/$', process_crime_areas, name='process_crime_areas'),
    url(r'^process_health_areas/$', process_health_areas, name='process_health_areas'),

    url(r'^process_person_name/$', process_person_name, name='process_person_names'),

    url(r'^process_dates/$', process_dates, name='process_person_names'),

    # word searches
    url(r'^test/$', test, name='test'),
    url(r'^testdates/$', test_dates, name='test_dates'),

    url(r'^process_identifiers/$', process_identifiers, name='process_identifiers'),

    # Yay, tetris
    url(r'^tetris/$', tetris, name='tetris')
]
