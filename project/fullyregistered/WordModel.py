# Submission Summary Model
import nltk
from nltk.corpus import names
import json
import datetime
from nltk import word_tokenize
from nltk.tag import pos_tag
import dateparser

from fullyregistered.models import OriginalSubmission, OriginalSubmissionPage, CrimeRecord, WordModelPerson, WordModelDate

DAYS_IN_MONTH = {"January": 31, "February": 29, "March": 31,
          "April": 30, "May": 31, "June": 30,
          "July": 31, "August": 31, "September": 30,
          "October": 31, "November": 30, "December": 31}

NAME_EXCEPTIONS = ["Victoria", "Royal", "Vic", "vic"]


def count_words(submission):
    words = 0
    pages = 0
    for page in submission:
        pages += 1
        tokens = word_tokenize(page)
        words += len(tokens)
    return pages, words


# extract entities from text (organisation, persons, etc)
def extract_person_name(submission):
    # convert to set for better performance
    names_set = set(names.words())

    for submissionPage in OriginalSubmissionPage.objects.filter(submission=submission):
        print("Processing: " + submissionPage.submission.submission_name + '\n')
        # check for person names
        for sent in nltk.sent_tokenize(submissionPage.page_content):
            for chunk in nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(sent))):
                # print(chunk)
                if hasattr(chunk, '_label'):
                    if chunk._label == 'PERSON':
                        new_person = WordModelPerson(submissionPage=submissionPage,person=' '.join(c[0] for c in chunk.leaves()))
                        name_token = word_tokenize(new_person.person)
                        if name_token[0] in names_set and name_token[0] not in NAME_EXCEPTIONS:
                            #print(chunk._label, ' '.join(c[0] for c in chunk.leaves()))
                            new_person.save()


def extract_incident(submission):
    verbs = []
    for submissionPage in OriginalSubmissionPage.objects.filter(submission=submission,index=4):
        split = submissionPage.page_content.split()
        tagged_body = pos_tag(split)
        #print(tagged_body)
        for word, tag in tagged_body:
            if tag == 'VB':
                try:
                    verbs.append(word)
                except UnicodeEncodeError:
                    pass
        print(verbs)


def is_leap_year(year):
    if (year % 4 is 0 and year % 100 is not 0) or year % 400 is 0:
        return True
    else:
        return False


# args: list of submission pages
def process_identifiers(submission):
    identifier = None
    for submissionPage in OriginalSubmissionPage.objects.filter(submission=submission):
        words = nltk.word_tokenize(submissionPage.page_content)
        # iterate through tokenised words
        for word in words:
            if 'SUBM' in word and len(word) == 18:
                identifier = word
                break
        if identifier:
            print("Found " + identifier + " for " + submissionPage.submission.submission_name + '\n')
            sub_page = submissionPage.submission
            sub_page.submission_identifier = identifier
            sub_page.save()

def find_date(submission):
    for submissionPage in OriginalSubmissionPage.objects.filter(submission=submission):
        #TODO dates spread over two pages won't be found
        words = nltk.word_tokenize(submissionPage.page_content)
        for i in range(len(words)):
            if words[i] in DAYS_IN_MONTH.keys():
                dateNumbers = []
                date_known = False
                year_known = False

                if (i - 1) <= (len(words) - 1) and words[i - 1].isdigit():
                    dateNumbers.append(words[i - 1])

                if (i + 1) <= (len(words) - 1) and words[i + 1].isdigit():
                    dateNumbers.append(words[i + 1])

                for item in dateNumbers:
                    if item.isdigit() and len(item) == 4:
                        year_known = True

                if year_known:
                    date_string = words[i] + ' ' + ' '.join(dateNumbers)
                    print(date_string)

                    datetimeObj = dateparser.parse(date_string)

                    if datetimeObj:
                        new_date = WordModelDate(submissionPage=submissionPage,
                                                 date=datetimeObj.date(),
                                                 is_year_known=year_known,
                                                 is_day_known=date_known)
                        new_date.save()

                # if len(dateNumber) > 3:
                #     year = dateNumber[0:4]
                #     year_known = True
                # elif len(dateNumber) <= 2:
                #     if day <= DAYS_IN_MONTH[words[i]]:
                #         day = dateNumber.zfill(2)  # fills beginning of string
                #         date_known = True

                # found_date = datetime.datetime.strptime(day+words[i]+year, "%d%B%Y").date()

