from django.db import models
from django.db.models import Model, TextField, ForeignKey, IntegerField, FloatField, URLField
import datetime

#Submissions
#http://www.rcfv.com.au/Submission-Review
class OriginalSubmission(Model):
    submission_name = TextField() #should this just be name
    submission_identifier = TextField(null=True)
    submitter = models.CharField(max_length=200, null=True)

    original_pdf_url = URLField(default='')

    def get_pages(self):
        return OriginalSubmissionPage.objects.filter(submission=self)

    def get_full_content(self):
        return ' '.join([page.page_content for page in self.originalsubmissionpage_set.all()])

    def preview(self):
        return self.originalsubmissionpage_set.first().page_content[:100]

    def getPeople(self):
        people = WordModelPerson.objects.filter(submissionPage__submission=self)

        person_list = [item.person for item in people]
        unique_person = set(person_list)

        return ', '.join(unique_person)

    def find_date_range(self):
        dates = WordModelDate.objects.filter(submissionPage__submission=self)
        datetime_list = [item.date for item in dates]

        if datetime_list:
            earliest = min(datetime_list)
            latest = max(datetime_list)

            try:
                result = earliest.strftime('%m/%d/%Y') + ', ' + latest.strftime('%m/%d/%Y')
                return earliest.strftime('%m/%d/%Y') + ', ' + latest.strftime('%m/%d/%Y')
            except:
                pass
        return ''

    def __str__(self):
        return self.submission_name + " -> " + self.originalsubmissionpage_set.first().page_content[:100]


class OriginalSubmissionPage(Model):
    class Meta:
        ordering = ['index']

    submission = ForeignKey(OriginalSubmission)
    page_content = TextField()
    index = IntegerField()

    def __str__(self):
        return self.submission.submission_name + " -> (Page " + str(self.index + 1) + ") " + self.page_content[:100]


class Address(Model):
    submission = ForeignKey(OriginalSubmission)
    address = TextField()
    lat = FloatField()
    lon = FloatField()

    def __str__(self):
        return str(self.submission) + " -> " + self.address


class PhoneNumber(Model):
    submission = ForeignKey(OriginalSubmission)
    number = TextField()
    city = TextField()
    lat = FloatField()
    lon = FloatField()

    def __str__(self):
        return str(self.submission) + " -> " + self.number

#Datasource of domestic incidents in area.
#https://www.data.vic.gov.au/data/dataset/data-tables-family-incidentss
class CrimeRecord(Model):
    region = TextField()
    area = TextField()
    incidents_2011 = IntegerField()
    rate_2011 = FloatField()
    incidents_2012 = IntegerField()
    rate_2012 = FloatField()
    incidents_2013 = IntegerField()
    rate_2013 = FloatField()
    incidents_2014 = IntegerField()
    rate_2014 = FloatField()
    incidents_2015 = IntegerField()
    rate_2015 = FloatField()

    lat = FloatField(default=0.0)
    lon = FloatField(default=0.0)

    def stats_2015(self):
        return str(self.incidents_2015) + " (" + str(self.rate_2015) + ")"

    def position(self):
        return str(self.lat) + ' and ' + str (self.lon)

    def __str__(self):
        return self.region + " - " + self.area + " -> " + str(self.incidents_2015) + " (" + str(self.rate_2015) + ")"

#Datasource? Of health services
#http://www.dvrcv.org.au/statewide
class HealthService(Model):
    region = TextField()
    name = TextField()
    is_aboriginal = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class HealthServiceArea(Model):
    health_service = ForeignKey(HealthService)
    #todo: merge the areas list from crimes and this?
    area = TextField()
    lat = FloatField(default=0.0)
    lon = FloatField(default=0.0)

    def position(self):
        return str(self.lat) + ' and ' + str (self.lon)

    def __str__(self):
        return self.area

#Australian Statisics domestic violence crime
#http://www.abs.gov.au/AUSSTATS/abs@.nsf/DetailsPage/4519.02014-15?OpenDocument


#word model tables for found details of documents.
class WordModelDate(Model):
    submissionPage = ForeignKey(OriginalSubmissionPage)
    date = models.DateField()
    is_day_known = models.BooleanField(default=False)
    is_year_known = models.BooleanField(default=False)

    def submission(self):
        return self.submissionPage.submission.submission_name

    def __str__(self):
        return str(self.date)

class WordModelPerson(Model):
    submissionPage = ForeignKey(OriginalSubmissionPage)
    person = models.CharField(max_length=200)

    def submission(self):
        return self.submissionPage.submission.submission_name

    def __str__(self):
        return self.person

class WordModelIncident(Model):
    submissionPage = ForeignKey(OriginalSubmissionPage)
    incident = models.CharField(max_length=200)

class WordModelOrganisation(Model):
    submissionPage = ForeignKey(OriginalSubmissionPage)
    organisation = models.CharField(max_length=200)